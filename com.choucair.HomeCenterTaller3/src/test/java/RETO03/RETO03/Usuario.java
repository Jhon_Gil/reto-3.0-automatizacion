package RETO03.RETO03;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;



public class Usuario {
 
    private String Nombres;
    private String Primer_Apellido;
    private String Segundo_Apellido;
    private String Tipo_Documento;
    private String Numero_Documento;
    private String Fecha_Incorrecta;
    private String Fecha_Correcta;

    
    public Usuario(String nombres, String primer_apellido, String segundo_apellido, String tipo_documento,String numero_documento,String fecha_incorrecta,String fecha_correcta ) {
        
    	setNombres(nombres);
        setPrimer_Apellido(primer_apellido);
        setSegundo_Apellido(segundo_apellido);
        setTipo_Documento(tipo_documento);
        setNumero_Documento(numero_documento);
        setFecha_Incorrecta(fecha_incorrecta);
        setFecha_Correcta(fecha_correcta);
        
    }
 
  

    public String getNombres() {
        return Nombres;
    }
 
    public void setNombres(String nombres) {
        this.Nombres = nombres;
    }
 
    public String getPrimer_Apellido() {
        return Primer_Apellido;
    }
 
    public void setPrimer_Apellido(String primer_apellido) {
        this.Primer_Apellido = primer_apellido;
    }
 
    public String getSegundo_Apellido() {
        return Segundo_Apellido;
    }
 
    public void setSegundo_Apellido(String Segundo_apellido) {
        this.Segundo_Apellido = Segundo_apellido;
    }
    public String getTipo_Documento() {
        return Tipo_Documento;
    }
 
    public void setTipo_Documento(String tipo_documento) {
        this.Tipo_Documento = tipo_documento;
    }
     
    public String getNumero_Documento() {
        return Numero_Documento;
    }
 
    public void setNumero_Documento(String numero_documento) {
        this.Numero_Documento = numero_documento;
    }
    
    public String getFecha_Incorrecta() {
        return Fecha_Incorrecta;
    }
 
    public void setFecha_Incorrecta(String fecha_incorrecta) {
        this.Fecha_Incorrecta = fecha_incorrecta;
    }
    public String getFecha_Correcta() {
        return Fecha_Correcta;
    } 
    
    public void setFecha_Correcta(String fecha_correcta) {
        this.Fecha_Correcta = fecha_correcta;
    }
    
    
}