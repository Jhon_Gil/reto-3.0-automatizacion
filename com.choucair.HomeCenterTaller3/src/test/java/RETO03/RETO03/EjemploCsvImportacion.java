package RETO03.RETO03;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.csvreader.CsvReader;

import RETO03.RETO03.Usuario;
public class EjemploCsvImportacion {
	
	private void cargarDatos() throws Throwable{	     
        try {
         
        List<Usuario> usuarios = new ArrayList<Usuario>();
         
        CsvReader usuarios_import = new CsvReader("D:\\Choucair\\Capacitacion\\Cursos\\Automatizacion1\\3\\com.choucair.HomeCenterTaller3\\com.choucair.HomeCenterTaller3\\src\\test\\resources\\usuarios_import.csv");
        usuarios_import.readHeaders();
         
     
        
        while (usuarios_import.readRecord())
        {
            String nombres = usuarios_import.get("Nombres");
            String primer_apellido = usuarios_import.get("Primer_Apellido");        
            String segundo_apellido = usuarios_import.get("Segundo_Apellido");
            String tipo_documento = usuarios_import.get("Tipo_Documento");
            String numero_documento = usuarios_import.get("Numero_Documento");
            String fecha_incorrecta = usuarios_import.get("Fecha_Incorrecta");
            String fecha_correcta = usuarios_import.get("Fecha_Correcta");
    
            usuarios.add(new Usuario(nombres, primer_apellido, segundo_apellido,tipo_documento,numero_documento,fecha_incorrecta,fecha_correcta));    
        }
        usuarios_import.close();
        
        for(Usuario us : usuarios){

         
            System.out.println(us.getNombres() + "," + us.getPrimer_Apellido() + ","+ us.getSegundo_Apellido() + ","+ us.getTipo_Documento() + ","
            + us.getNumero_Documento() + "," + us.getFecha_Incorrecta() + "," + us.getFecha_Correcta());
        }
         
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

