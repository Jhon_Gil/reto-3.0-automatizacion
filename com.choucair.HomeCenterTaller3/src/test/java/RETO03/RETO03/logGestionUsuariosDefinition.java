package RETO03.RETO03;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;
import java.util.Random;

public class logGestionUsuariosDefinition {

	@Steps
	logGestionUsuariosSteps Reto03steps;

	@Given("^Ingresar pagina Proteccion-Capturar y mostrar en pantalla de ejecución el punto (\\d+) de las condiciones de uso$")
	public void ingresar_pagina_Proteccion_Capturar_y_mostrar_en_pantalla_de_ejecución_el_punto_de_las_condiciones_de_uso(
			int arg1) {
		Reto03steps.ingresaralAplicativoProteccion();
	}

	@Given("^Llenar nombres, apellido y segundo apellido$")
	public void llenarNombresApellidoySegundoApellido() {
		Reto03steps.registrarDatos();

	}

	@Given("^Ir al link …más ventajas$")
	public void iralLinkmásVentajas() {
		Reto03steps.registrarDatos();

	}

	@Given("^Llenar el tipo y número de documento$")
	public void llenarelTipoyNúmerodeDocumento() {
		Reto03steps.registrarDatos();

	}

	@Given("^Llenar el datePicker fecha Exp$")
	public void llenarelDatePickerFechaExp() {
		Reto03steps.registrarDatos();

	}

	@Then("^Controlar la excepción de la fecha y mostrar el caso como exitoso$")
	public void controlarlaExcepcióndelaFechayMostrarelCasocomoExitoso() {
		Reto03steps.registrarDatos();

	}

	// @Given("^Registrar Datos$")
	// public void registrar_Datos() throws Exception {
	// Reto03steps.Registrar_Datos();
	// }

}