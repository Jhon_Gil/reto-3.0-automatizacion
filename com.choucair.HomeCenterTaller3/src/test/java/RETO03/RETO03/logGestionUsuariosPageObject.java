package RETO03.RETO03;

import static org.junit.Assert.fail;
import java.security.SecureRandom;
import java.util.Random;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.math.BigInteger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import com.gargoylesoftware.htmlunit.javascript.host.file.File;
import com.github.rjeschke.txtmark.Line;
import com.ibm.icu.impl.RelativeDateFormat.URelativeString;

import io.appium.java_client.ios.IOSDriver;
import jnr.ffi.Struct.key_t;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.pages.WebElementState;
import net.serenitybdd.screenplay.questions.JavaScript;
import net.thucydides.core.model.screenshots.Screenshot;
import java.util.*;

public class logGestionUsuariosPageObject extends PageObject {

	// public String Condicioness =
	// "//*[@id=\"section-main\"]/div/div[1]/div[1]/div/section/div/div[2]/div[4]/div/div";
	public String Condiciones = "//*[@id=\"section-main\"]/div/div[1]/div[1]/div/section/div/div[2]/div[4]/div";
	public String TextoCondiciones = "//*[@id=\"formularioAutorizarCondUso\"]/div/div[3]";
	public String LblNombres = "//*[@id=\"nombres\"]";
	public String LblPrimerApellido = "//*[@id=\"primerApellido\"]";
	public String LblsegundoApellido = "//*[@id=\"segundoApellido\"]";
	public String BtnMasVentajas = "//*[@id=\"tab2\"]";
	public String TipoDocumento = "//*[@id=\"tipoIdentificacion\"]";
	public String NumeroDocumento = "//*[@id=\"identificacion\"]";
	public String Fecha = "//*[@id=\"fechaExpedicion\"]";
	public String Boton = "//*[@id=\"izquierdoAct\"]/div[5]/a";
	public String Boton2 = "//*[@id=\"popup_ok\"]";
	public String Boton3 = "//*[@id=\"popup_ok\"]";

	public void conectar() {
		try {
			if (!Condiciones.isEmpty()) {

				find(By.xpath(Condiciones)).click();
				Serenity.takeScreenshot();
				PosicionarElementoString(TextoCondiciones);
				Serenity.takeScreenshot();
				find(By.xpath(TextoCondiciones)).isVisible();
				getDriver().navigate().back();
				// System.out.println(find(By.xpath(Solicitar_Clave)).getText());
				// WebElement textbox = getDriver().findElement(By.xpath(Solicitar_Clave));
				// textbox.sendKeys("Hello World", Keys.ENTER);
				// waitFor((WebElement)find(By.xpath(Solicitar_Clave))).sendKeys(Keys.chord(Keys.CONTROL
				// + "a")+Keys.TAB);
			}
		} catch (Exception e) {
			fail("error en la clase logGestionUsuariosPageObjet, en el metodo conectar: " + e);
		}
	}

	public void RegistrarNombres(String nombre) {
		try {
			if (!Condiciones.isEmpty()) {

				// switchearVentanaMultiple("Solicita tu Clave");
				// getDriver().switchTo().defaultContent();
				// getDriver().switchTo().frame(0);
				// getDriver().switchTo().frame(1);
				getDriver().switchTo().frame("contenido");
				getDriver().switchTo().frame("contenido2");
				System.out.println(find(By.xpath(LblNombres)).getText());
				getDriver().findElements(By.xpath(LblNombres)).get(0).click();
				find(By.xpath(LblNombres)).clear();
				find(By.xpath(LblNombres)).sendKeys(nombre);
				find(By.xpath(LblNombres)).isVisible();

			}
		} catch (Exception e) {
			fail("error en la clase logGestionUsuariosPageObjet, en el metodo RegistrarNombres: " + e);
		}

	}

	public void PrimerApellido(String PrimerApellido) {
		try {
			if (!LblPrimerApellido.isEmpty()) {

				getDriver().findElements(By.xpath(LblPrimerApellido)).get(0).click();
				find(By.xpath(LblPrimerApellido)).clear();
				find(By.xpath(LblPrimerApellido)).sendKeys(PrimerApellido);
				find(By.xpath(LblPrimerApellido)).isVisible();
			}
		} catch (Exception e) {
			fail("error en la clase logGestionUsuariosPageObjet, en el metodo PrimerApellido: " + e);
		}
	}

	public void SegundoApellido(String SegundoApellido) {
		try {
			if (!LblsegundoApellido.isEmpty()) {

				getDriver().findElements(By.xpath(LblsegundoApellido)).get(0).click();
				find(By.xpath(LblsegundoApellido)).clear();
				find(By.xpath(LblsegundoApellido)).sendKeys(SegundoApellido);
				find(By.xpath(LblsegundoApellido)).isVisible();
			}
		} catch (Exception e) {
			fail("error en la clase logGestionUsuariosPageObjet, en el metodo SegundoApellido: " + e);
		}

	}

	public void TipoDocumento(String Tipo_Documento) {
		try {
			if (!BtnMasVentajas.isEmpty()) {

				find(By.xpath(BtnMasVentajas)).click();
				find(By.xpath(BtnMasVentajas)).isVisible();

				getDriver().findElements(By.xpath(TipoDocumento)).get(0).click();
				find(By.xpath(TipoDocumento)).sendKeys(Tipo_Documento);
				find(By.xpath(TipoDocumento)).isVisible();
			}
		} catch (Exception e) {
			fail("error en la clase logGestionUsuariosPageObjet, en el metodo TipoDocumento: " + e);
		}
	}

	public void NumeroDocumento(String Numero_Documento) {
		try {
			if (!NumeroDocumento.isEmpty()) {

				getDriver().findElements(By.xpath(NumeroDocumento)).get(0).click();
				find(By.xpath(NumeroDocumento)).clear();
				find(By.xpath(NumeroDocumento)).sendKeys(Numero_Documento);
				find(By.xpath(NumeroDocumento)).isVisible();
			}
		} catch (Exception e) {
			fail("error en la clase logGestionUsuariosPageObjet, en el metodo NumeroDocumento: " + e);
		}
	}

	public void LlenarDatePickerFechaExp(String Fecha_Incorrecta) {
		try {
			if (!Fecha.isEmpty()) {

				getDriver().findElements(By.xpath(Fecha)).get(0).click();
				find(By.xpath(Fecha)).click();
				find(By.xpath(Fecha)).clear();
				find(By.xpath(Fecha)).sendKeys(Fecha_Incorrecta);
				find(By.xpath(Fecha)).isVisible();
			}
		} catch (Exception e) {
			fail("error en la clase logGestionUsuariosPageObjet, en el metodo LlenarDatePickerFechaExp: " + e);
		}
	}

	public void ControlarlaExcepcióndeLafechaMostrarcasoExitoso(String Fecha_Correcta) {
		try {
			if (!Boton2.isEmpty()) {
				find(By.xpath(Boton)).click();
				// find(By.xpath(Boton)).isVisible();
				find(By.xpath(Boton2)).click();
				find(By.xpath(Fecha)).click();
				find(By.xpath(Fecha)).clear();
				find(By.xpath(Fecha)).sendKeys(Fecha_Correcta);
				find(By.xpath(Fecha)).isVisible();
				find(By.xpath(Boton)).click();
				find(By.xpath(Boton2)).click();

			}
		} catch (Exception e) {
			fail("error en la clase logGestionUsuariosPageObjet, en el metodo ControlarlaExcepcióndeLafechaMostrarcasoExitoso: "
					+ e);
		}

	}

	public void PosicionarElementoString(String Element) {
		try {
			((JavascriptExecutor) getDriver()).executeScript(
					"arguments[0].scrollIntoView(true); arguments[0].style.border='1px dashed red';",
					getDriver().findElements(By.xpath(Element)).get(0));
		} catch (Exception e) {
			fail("error en la clase logGestionUsuariosPageObjet, en el metodo PosicionarElementoString " + ": " + e);
		}
	}

}
