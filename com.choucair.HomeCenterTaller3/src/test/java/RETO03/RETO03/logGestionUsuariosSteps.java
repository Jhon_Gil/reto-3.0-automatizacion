package RETO03.RETO03;


import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

import au.com.bytecode.opencsv.CSVReader;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class logGestionUsuariosSteps {

	logGestionUsuariosPageObject Reto03pageobjet;

	@Step
	public void ingresaralAplicativoProteccion() {
		String url = "https://www.proteccion.com/wps/portal/proteccion/web/home/general/solicita-clave";
		Reto03pageobjet.openUrl(url);
        Reto03pageobjet.conectar();
	}

//	@Step
//	public void registrarDatoas() {
//		try {
//
//			List<Usuario> usuarios = new ArrayList<Usuario>();
//
//			CsvReader usuarios_import = new CsvReader("src\\test\\resources\\usuarios_import.csv");
//			usuarios_import.readHeaders();
//
//			while (usuarios_import.readRecord()) {
//				String Nombres = usuarios_import.get("Nombres");
//				String PrimerApellido = usuarios_import.get("Primer_Apellido");
//				String SegundoApellido = usuarios_import.get("Segundo_Apellido");
//				String TipoDocumento = usuarios_import.get("Tipo_Documento");
//				String NumeroDocumento = usuarios_import.get("Numero_Documento");
//				String FechaIncorrecta = usuarios_import.get("Fecha_Incorrecta");
//				String FechaCorrecta = usuarios_import.get("Fecha_Correcta");
//
//				usuarios.add(new Usuario(Nombres, PrimerApellido, SegundoApellido, TipoDocumento, NumeroDocumento,
//						FechaIncorrecta, FechaCorrecta));
//			}
//			usuarios_import.close();
//
//			for (Usuario us : usuarios) {
//
//				// System.out.println(us.getNombres() + "," + us.getPrimer_Apellido() + "," +
//				// us.getSegundo_Apellido()
//				// + "," + us.getTipo_Documento() + "," + us.getNumero_Documento() + "," +
//				// us.getFecha_Incorrecta()
//				// + "," + us.getFecha_Correcta());
//
//				Reto03pageobjet.RegistrarNombres(us.getNombres());
//				Reto03pageobjet.PrimerApellido(us.getPrimer_Apellido());
//				Reto03pageobjet.SegundoApellido(us.getSegundo_Apellido());
//				Reto03pageobjet.TipoDocumento(us.getTipo_Documento());
//				Reto03pageobjet.NumeroDocumento(us.getNumero_Documento());
//				Reto03pageobjet.LlenarDatePickerFechaExp(us.getFecha_Incorrecta());
//				Reto03pageobjet.ControlarlaExcepcióndeLafechaMostrarcasoExitoso(us.getFecha_Correcta());
//
//			}
//
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//	}
	@Step
	public void registrarDatos() {
		System.out.println("CARGAR ENTRO A CARGAR DATOS");
		String CSV_file = "src\\test\\resources\\usuarios_import.csv"; // archivo csv de donde se leen
																						// los datos
	    //String carpeta = "ObtenerDatos";
		//String archivo = "TrasladoAFP";
		//String strRutaPropAcreditar="src\\test\\resources\\properties\\'" + carpeta
		// +"'\\'"+ archivo +"'.properties";
		//String strRutaPropAcreditar = "src\\test\\resources\\usuarios_import.csv";
		CSVReader reader = null;
		try {	
			reader = new CSVReader(new FileReader(CSV_file));
			String[] cell = reader.readNext();

			while ((cell = reader.readNext()) != null) {
				System.out.println("CARGAR ENTRO A CARGAR DATOS - LOS LEYO");
				int i = 0;
				String Nombres = cell[i];
				String Primer_Apellido = cell[i + 1];
				String Segundo_Apellido = cell[i + 2];
				String Tipo_Documento = cell[i + 3];
				String Numero_Documento = cell[i + 4];
				String Fecha_Incorrecta = cell[i + 5];
				String Fecha_Correcta = cell[i + 6];	
				//String Nombre2 = cell[i + 7];
				
				Reto03pageobjet.RegistrarNombres(Nombres);
				Reto03pageobjet.PrimerApellido(Primer_Apellido);
				Reto03pageobjet.SegundoApellido(Segundo_Apellido);
				Reto03pageobjet.TipoDocumento(Tipo_Documento);
				Reto03pageobjet.NumeroDocumento(Numero_Documento);
				Reto03pageobjet.LlenarDatePickerFechaExp(Fecha_Incorrecta);
				Reto03pageobjet.ControlarlaExcepcióndeLafechaMostrarcasoExitoso(Fecha_Correcta);
				//Reto03pageobjet.RegistrarNombres2(Nombre2);
				

				
				
			}
		} catch (Exception e) {
			fail("error en la clase logGestionUsuariosPageObjet, en el metodo PosicionarElementoString " + ": " + e);
		}
	}
}
